use crate::order::{Order, OrderSide};
use crate::transaction::Transaction;
use std::cmp::Ordering;
use std::collections::BinaryHeap;

#[derive(Debug)]
pub struct OrderBook {
    // Using priority queues for efficient autosorting.
    // If orders had ids and it was necessary to remove or
    // find a specific order though this would need another
    // solution as BinaryHeaps currently don't support that
    // (https://github.com/rust-lang/rust/issues/82001).
    // Probably would make them sorted vecs and do binary search in that case.
    sell_orders: BinaryHeap<SellBookRecord>,
    buy_orders: BinaryHeap<BuyBookRecord>,
}

// I'm adding placeholder for timestamps here as it occurred to me that
// in case of prices being equal it should probably be first came
// first served.
// Also assuming prices to be integers

#[derive(Debug, Eq, PartialEq)]
pub struct BookRecord {
    price: u32,
    ts: u32,
    client: String,
    amount: u32,
}

// All this boilerplate is basically to be able to use differing ordering for
// the sell_orders and buy_orders priority queues (simple reversing is not enough
// as it would handle the timestamps incorrectly). There probably is some better way
// but I'm not really sure at the moment how to handle this with no inheritance,
// while still restricting each queue to a wrapper type with its own ordering
// implementation. Also not having access to fields from trait definitions
// is unfortunate.

trait BookRecordWrapper {
    fn from_order(order: Order) -> Self;
}

#[derive(Debug, Eq, PartialEq)]
pub struct SellBookRecord {
    pub record: BookRecord,
}

#[derive(Debug, Eq, PartialEq)]
pub struct BuyBookRecord {
    pub record: BookRecord,
}

impl PartialOrd for SellBookRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SellBookRecord {
    fn cmp(&self, other: &Self) -> Ordering {
        self.record
            .price
            .cmp(&other.record.price)
            .reverse()
            .then_with(|| self.record.ts.cmp(&other.record.ts).reverse())
    }
}

impl BookRecordWrapper for SellBookRecord {
    fn from_order(order: Order) -> Self {
        Self {
            record: BookRecord::from_order(order),
        }
    }
}

impl PartialOrd for BuyBookRecord {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for BuyBookRecord {
    fn cmp(&self, other: &Self) -> Ordering {
        self.record
            .price
            .cmp(&other.record.price)
            .then_with(|| self.record.ts.cmp(&other.record.ts).reverse())
    }
}

impl BookRecordWrapper for BuyBookRecord {
    fn from_order(order: Order) -> Self {
        Self {
            record: BookRecord::from_order(order),
        }
    }
}

impl BookRecord {
    pub fn from_order(order: Order) -> BookRecord {
        BookRecord {
            price: order.price,
            client: order.client,
            amount: order.amount,
            ts: 0, // just fake it atm for the requested functionality
        }
    }
}

impl OrderBook {
    pub fn new() -> OrderBook {
        OrderBook {
            sell_orders: BinaryHeap::new(),
            buy_orders: BinaryHeap::new(),
        }
    }

    pub fn push_order(&mut self, order: Order) {
        match order.side {
            OrderSide::Buy => {
                self.buy_orders.push(BuyBookRecord::from_order(order));
            }
            OrderSide::Sell => {
                self.sell_orders.push(SellBookRecord::from_order(order));
            }
        }
    }

    // to test additional timestamp ordering
    pub fn push_order_with_ts(&mut self, order: Order, ts: u32) {
        match order.side {
            OrderSide::Buy => {
                let mut br = BuyBookRecord::from_order(order);
                br.record.ts = ts;
                self.buy_orders.push(br);
            }
            OrderSide::Sell => {
                let mut br = SellBookRecord::from_order(order);
                br.record.ts = ts;
                self.sell_orders.push(br);
            }
        }
    }

    // Assuming this should return an absolute value
    // (and also that the difference should be calculated against 0 if
    // there is no orders on either side, as seen in the example)
    pub fn get_spread(&self) -> u32 {
        (i64::from(self.buy_orders.peek().map_or(0, |v| v.record.price))
            - i64::from(self.sell_orders.peek().map_or(0, |v| v.record.price)))
        .abs() as u32
    }

    pub fn process(&mut self) -> Vec<Transaction> {
        let mut txs = Vec::new();
        loop {

            match (self.buy_orders.peek(), self.sell_orders.peek()) {
                (Some(buy_record), Some(sell_record)) => {
                    if sell_record.record.price <= buy_record.record.price {
                        let ask = self.sell_orders.pop().unwrap().record;
                        let bid = self.buy_orders.pop().unwrap().record;
                        let mut tx = Transaction {
                            seller: ask.client.clone(),
                            buyer: bid.client.clone(),
                            amount: 0,
                            price: ask.price,
                        };

                        match i64::from(ask.amount) - i64::from(bid.amount) {
                            d if d < 0 => {
                                // bid amount was larger, amend the record and push it back
                                let mut leftover = bid;
                                leftover.amount = leftover.amount - ask.amount;
                                self.buy_orders.push(BuyBookRecord { record: leftover });
                                tx.amount = ask.amount;
                            }
                            d if d > 0 => {
                                // ask amount was larger, amend the record and push it back
                                let mut leftover = ask;
                                leftover.amount = leftover.amount - bid.amount;
                                self.sell_orders.push(SellBookRecord { record: leftover });
                                tx.amount = bid.amount;
                            }
                            _ => {
                                // amounts were equal, don't push back anything
                                tx.amount = ask.amount;
                            }
                        }

                        txs.push(tx);
                    } else {
                        // no legitimate deals present at the moment
                        break;
                    }
                }
                // at least one of the queues is empty
                _ => break,
            }
        }

        txs
    }

    // convenient for testing
    pub fn get_queue_lengths(&self) -> (usize, usize) {
        (self.buy_orders.len(), self.sell_orders.len())
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_orderbook() {
        let orderbook = OrderBook::new();
        assert_eq!((0, 0), orderbook.get_queue_lengths());
    }

    #[test]
    fn integration_test_orders() {
        let mut orderbook = OrderBook::new();

        orderbook.push_order(Order{client: String::from("Alice"), side: OrderSide::Sell, amount: 1, price: 42});
        assert_eq!(42, orderbook.get_spread());
        orderbook.push_order(Order{client: String::from("Bob"), side: OrderSide::Sell, amount: 10, price: 43});
        assert_eq!(42, orderbook.get_spread());
        orderbook.push_order(Order{client: String::from("John"), side: OrderSide::Buy, amount: 9, price: 43});
        assert_eq!(1, orderbook.get_spread());
        
        assert_eq!((1, 2), orderbook.get_queue_lengths());

        let alice_john_tx = Transaction{seller: String::from("Alice"), buyer: String::from("John"), amount: 1, price: 42};
        let bob_john_tx = Transaction{seller: String::from("Bob"), buyer: String::from("John"), amount: 8, price: 43};
        let correct_tx_vec: Vec<Transaction> = vec![alice_john_tx, bob_john_tx];
        assert_eq!(correct_tx_vec, orderbook.process())
    }

    #[test]
    fn buy_price_lower_than_sell() {
        let mut orderbook = OrderBook::new();
        orderbook.push_order(Order{client: String::from("Alice"), side: OrderSide::Sell, amount: 1, price: 42});
        orderbook.push_order(Order{client: String::from("Bob"), side: OrderSide::Buy, amount: 1, price: 41});
        let empty_tx_vec: Vec<Transaction> = Vec::new();
        assert_eq!(empty_tx_vec, orderbook.process())
    }

    #[test]
    fn buy_price_equal_or_higher_than_sell() {
        let mut orderbook = OrderBook::new();
        orderbook.push_order(Order{client: String::from("Alice"), side: OrderSide::Sell, amount: 2, price: 42});
        orderbook.push_order(Order{client: String::from("Bob"), side: OrderSide::Buy, amount: 1, price: 42});
        orderbook.push_order(Order{client: String::from("John"), side: OrderSide::Buy, amount: 1, price: 43});
        let correct_tx_vec: Vec<Transaction> = vec![
            Transaction{seller: String::from("Alice"), buyer: String::from("John"), amount: 1, price: 42},
            Transaction{seller: String::from("Alice"), buyer: String::from("Bob"), amount: 1, price: 42}
        ];
        assert_eq!(correct_tx_vec, orderbook.process())
    }

    #[test]
    fn test_orderbook_get_spread() {
        let mut orderbook: OrderBook = OrderBook::new();
        orderbook.push_order(Order{client: String::from("Bob"), side: OrderSide::Sell, amount: 1, price: 100});
        orderbook.push_order(Order{client: String::from("John"), side: OrderSide::Buy, amount: 1, price: 50});
        assert_eq!(50, orderbook.get_spread())
    }

    #[test]
    fn test_orderbook_get_spread_when_one_order_is_empty() {
        let mut orderbook: OrderBook = OrderBook::new();
        orderbook.push_order(Order{client: String::from("Bob"), side: OrderSide::Sell, amount: 1, price: 100});
        assert_eq!(100, orderbook.get_spread())
    }
}
