pub enum OrderSide {
    Sell,
    Buy,
}

pub struct Order {
    pub price: u32,
    pub amount: u32,
    pub side: OrderSide,
    pub client: String,
}
