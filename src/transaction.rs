use std::fmt;

#[derive(PartialEq, Eq)]
pub struct Transaction {
    pub seller: String,
    pub buyer: String,
    pub amount: u32,
    pub price: u32,
}

impl fmt::Debug for Transaction {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_fmt(format_args!(
            "Tx {{ {} -> {}, amount = {}, price = {} }}",
            self.seller, self.buyer, self.amount, self.price
        ))?;
        Ok(())
    }
}
